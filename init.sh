#!/usr/bin/env bash

export LANG=C
export GROUP=evolved
export ALTER=mana

CMD="$1"
PROTO="https://git.themanaworld.org/"
if [[ -z "${CMD}" ]]; then
    export CMD="default"
fi

CLONECMD="git clone --origin upstream"
CLONE1="${CLONECMD} ${PROTO}${GROUP}"
CLONE2="${CLONECMD} ${PROTO}${ALTER}"

if [[ "${CMD}" == "all" || "${CMD}" == "default" ]]; then
    ${CLONE1}/clientdata.git client-data
    ${CLONE1}/hercules.git server-code
    ${CLONE1}/serverdata.git server-data
    ${CLONE1}/tools.git tools
    ${CLONE1}/evol-hercules.git server-code/src/evol
    cd server-code/src/evol ; cd ../../..
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "server" ]]; then
    ${CLONE1}/hercules.git server-code
    ${CLONE1}/serverdata.git server-data
    ${CLONE1}/evol-hercules.git server-code/src/evol
    cd server-code/src/evol ; cd ../../..
    ${CLONE1}/tools.git tools
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "client" ]]; then
    ${CLONE1}/clientdata.git client-data
    ${CLONE1}/tools.git tools
#elif [[ "${CMD}" == "music" ]]; then
#    ${CLONE1}/evol-music.git music
elif [[ "${CMD}" == "local" ]]; then
    ${CLONE1}/hercules.git server-code
    ${CLONE1}/serverdata.git server-data
    ${CLONE1}/evol-hercules.git server-code/src/evol
    cd server-code/src/evol ; cd ../../..
    ${CLONE1}/tools.git tools
    ln -s server-code/src/evol server-plugin
elif [[ "${CMD}" == "tools" ]]; then
    ${CLONE1}/tools.git tools
elif [[ "${CMD}" == "plugin" ]]; then
    ${CLONE1}/evol-hercules.git server-code/src/evol
    cd server-code/src/evol ; cd ../../..
    ln -s server-code/src/evol server-plugin
#elif [[ "${CMD}" == "docs" ]]; then
#    ${CLONE1}/Docs.git docs
#    ${CLONE1}/Docs.wiki.git wiki
elif [[ "${CMD}" == "manaplus" ]]; then
    ${CLONE2}/plus.git manaplus
#elif [[ "${CMD}" == "media" ]]; then
#    ${CLONE1}/art.git art
#elif [[ "${CMD}" == "mods" ]]; then
#    ${CLONE1}/mods.git client-data/mods
elif [[ "${CMD}" == "update" ]]; then
    ${CLONE1}/hercules.git server-code
    ${CLONE1}/evol-hercules.git server-code/src/evol
    cd server-code/src/evol ; cd ../../..
    ln -s server-code/src/evol server-plugin
fi

#if [[ "${CMD}" == "all" ]]; then
#    ${CLONE1}/evol-music.git music
#    ${CLONE1}/mods.git client-data/mods
#    ln -s music/music client-data/
#    ${CLONE2}plus.git manaplus
#fi
