sudo apt-get install -y git g++ gcc xsel make autoconf automake autopoint gettext libxml2-dev libcurl4-gnutls-dev libpng-dev libsdl-gfx1.2-dev libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-net1.2-dev libsdl-ttf2.0-dev gdb valgrind netcat-openbsd procps zlibc
git clone https://git.themanaworld.org/mana/plus.git manaplus
cd manaplus
autoreconf -i
./configure -q
make -s
sudo make install
